#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import cv2
from scipy.misc import imread, imsave, imresize
import matplotlib.pyplot as plt


# In[1]:


def prg(img):
    dst = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 15) 
    mask=cv2.inRange(dst,(0,0,0),(150,180,180))
    res=255-cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
    return res


# In[ ]:




