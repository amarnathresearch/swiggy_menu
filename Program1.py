#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import cv2
from scipy.misc import imread, imsave, imresize
import matplotlib.pyplot as plt


# In[4]:


def prg(img):
    ret, outputimg = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return outputimg


# In[9]:


# img = cv2.imread('dummy/1.png',0)
# outputimg = prg(img)
# cv2.imwrite("dummy/1-bin.png", outputimg)


# In[ ]:




