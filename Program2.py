#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import cv2
import img2pdf 
from PIL import Image 
import os
import math


# In[2]:



def findThreshold(T, grayImg, height, width):
    for i in range(0, height):
        for j in range(0, width):
            # threshold the pixel
            if grayImg[i, j] >= T:
                grayImg[i, j] = 255
            else:
                grayImg[i, j] = 0
    
    return grayImg
def saveImgToPdf(imoutname, pdfoutname, image):
    pdf_path = pdfoutname
    # converting into chunks using img2pdf 
    image = Image.open(imoutname) 
    pdf_bytes = img2pdf.convert(image.filename) 

    # opening or creating pdf file 
    file = open(pdf_path, "wb") 

    # writing pdf files with chunks 
    file.write(pdf_bytes) 

    # closing image file 
    image.close() 

    # closing pdf file 
    file.close() 

def saveImg(imagename, img):
    cv2.imwrite(imagename, img)
    cv2.waitKey(0) & 0xFF
    cv2.destroyAllWindows()
def calSize(t, width, height):
    if height > width:
        if width > t:
            tw = width/t
            width = t
            height = math.floor(height/tw)
    else:
        if height > t:
            th = height/t
            height = t
            width = math.floor(width/th)
    return width, height
def findComponent(img, min_size):
    img = ~img
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(img, connectivity=8)
    sizes = stats[1:, -1]; nb_components = nb_components - 1
    img2 = np.zeros((output.shape))
    for i in range(0, nb_components):
        if sizes[i] >= min_size:
            img2[output == i + 1] = 255
            
    return img2
def removeComponent(img, large, width, height):
    for i in range(0, height):
        for j in range(0, width):
            # threshold the pixel
            if large[i, j] >= 250:
                img[i, j] = 255
    return img
def finalMatch(img, large, width, height):
    for i in range(0, height):
        for j in range(0, width):
            # threshold the pixel
            if img[i, j] == 0 and large[i, j] == 0:
                img[i, j] = 0
            else:
                img[i, j] = 255
    return img




# In[13]:


# path = 'V Royal Park_8/'
# imname = path+'input.png'
# imoutname = path+'output.png'
# pdfoutname = path+'output.pdf'
# # Load an color image in grayscale
# img = cv2.imread(imname, 5)

def prg(img):
    height, width, channels = img.shape
    width, height = calSize(600, width, height)
    dst = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 15)     
    blue, green, red = cv2.split(dst)

    # img2 = cv2.merge((blue, green, red))
    red = cv2.resize(red, (width, height))

    T = 150
    timg = findThreshold(T, red, height, width)

    #Dilation
    kernel = np.ones((2,2), np.uint8)
    dilate = ~cv2.dilate(~timg, kernel, iterations=1) 


    #find largest component

    largest = findComponent(dilate, 1500)
    kernel = np.ones((10, 10), np.uint8)

    #Dilate largest
    largest_dilate = cv2.dilate(largest, kernel, iterations=1) 
    # saveImg(imagename+'-out-large-dilate', largest_dilate)

    #Remove Largest component
    rc = removeComponent(dilate, largest_dilate, width, height)
    # saveImg(imagename+'-dilated', rc)

    #Just Threshold
    ft = findThreshold(T, red, height, width)
    # saveImg(imagename+'-ft', ft)

    #Final output
    fm = finalMatch(ft, rc, width, height)

    kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    im = cv2.filter2D(fm, -1, kernel)
    return im

# saveImg(imoutname, fm)
# saveImgToPdf(imoutname,pdfoutname, im)


# In[ ]:




