#!/usr/bin/env python
# coding: utf-8

# In[150]:


import re
import xlwt 
from xlwt import Workbook 
import subprocess as sp


# In[2]:


def findTesseract(inputname, outputname):
#     task = ["tesseract", inputname, outputname]
    task = ["tesseract", "-c", "preserve_interword_spaces=1", inputname, outputname, "-l", "eng", "--psm", "6"]
    print(task)
    sp.check_call(task)
def getList(inxls):
    f = open(inxls, 'r')
    raw = f.read()
    f.close()
    raw = raw.split('\n')
    txt = []
    for r in raw:
        if len(r.strip()) > 0:
            txt.append(r)
    res = []
    for t in txt:
#         print(t)
        sentences = t.split("   ")
        refinedSentence = []
        for s in sentences:
            if len(s.strip()) > 0:
                refinedSentence.append(s.strip())
        if len(refinedSentence) > 0:
            res.append(refinedSentence)
#                 print(s.strip())
    pr = []
    for r in res:
#         print(r)
        words = []
        for w in r:
            t1 = re.sub('[^A-Za-z0-9]+', '', w)
            if len(t1) > 0:
                words.append(w)
        pr.append(words)
    final = []
    for sen in pr:
#         print(sen)
        fin = []
        for s in sen:
            wd = s.split()
            digits = 0
            f = 0
            if len(wd[0]) > 1:
                for i in range(len(wd[0].strip())):
                    if(wd[0].strip()[i].isdigit()):
                        digits = digits + 1
                if digits >= len(wd[0].strip())/3:
                    f = 1
            if f == 0:
                if len(s.strip())>0:
                    fin.append(s.strip())
            else:
                fin.append(wd[0])
                stri = ""
                for k in range(len(wd)):
                    if k > 0:
                        stri = stri + " "+wd[k]
                if len(stri.strip())>0:
                    fin.append(stri)
#         print(fin)
        final.append(fin)

    complete = []
    for lis in final:
        com = []
        for li in lis:
            f = 1
            t1 = re.sub('[^A-Za-z0-9]+', '', li)
            if len(t1) == 1:
                if t1.isnumeric() == False:
                    f = 0
            if len(t1) == 2:
                if t1.isnumeric() == False:
                    f = 0
            if f == 1:
                com.append(li)
        if len(li) > 0:
            complete.append(com)
#     for c in complete:
#         print(c)
    return complete
# def getList(inxls):
#     f = open(inxls, 'r')
#     raw = f.read()
#     f.close()
#     raw = raw.split('\n')
#     txt = []
#     for r in raw:
#         if len(r.strip()) > 0:
#             txt.append(r)
#     ntxt = []
#     for t in txt:
#         t1 = re.sub('[^A-Za-z0-9]+', ' ', t)
#         if len(t1.strip()) > 0:
#             ntxt.append(t1)
#     items = []
#     prices = []
#     for n in ntxt:
#         words = n.split(' ')
#         tmp = ''
#         for w in words:
#             if w.isnumeric() == False:
#                 tmp = tmp + w +' '
#             else:
#                 if len(tmp.strip()) > 2:
#                     items.append(tmp.strip().title())
#                     tmp = ''
#                 prices.append(w)
#         if len(tmp.strip()) > 2:
#             items.append(tmp.strip().title())
#     return items, prices

def writeXLS(wb, outxls, sno, items):
    # Workbook is created
    if wb == 0:
        wb = Workbook()
    # add_sheet is used to create sheet. 
    sheet1 = wb.add_sheet('Sheet '+sno, cell_overwrite_ok=True)
    row = 0
    for lines in items:
        col = 0
        for sen in lines:
            sheet1.write(row, col, sen)
            col += 1
        row += 1
    return wb
            

# def writeXLS(wb, outxls, sno, items, prices):
#     # Workbook is created
#     if wb == 0:
#         wb = Workbook() 
#     # add_sheet is used to create sheet. 
#     sheet1 = wb.add_sheet('Sheet '+sno, cell_overwrite_ok=True)
#     row = 0
#     for it in items:
#         sheet1.write(row, 0, it)
#         row+=1
#     row = 0
#     for pr in prices:
#         sheet1.write(row, 1, pr)
#         row+=1
#     return wb
def saveXLS(wb, outxls):
    wb.save(outxls)


# In[152]:


# path = 'Tanish Dining & Cafe_9/'
# filename = '2'
# filename = 'Tanish Dining & Cafe_9-5'
# filename = 'Tanish Dining & Cafe_9-9'
# filename = '3'
# ext = '.png'
# outxls = path+filename+'.xls'
# inputname = path+filename+ext
# outputname = path+filename
# print("Start")
# findTesseract(inputname, outputname)
# print("End")
# inxls = path+filename+'.txt'
# items = getList(inxls)

# outxls = path+filename+'.xls'
# wb = 0
# wb = writeXLS(wb, outxls, '1', items)
# print(wb)
# saveXLS(wb, outxls)




# In[68]:


t = "285 Chef's Style Egg n Chicken Salad"
t1 = re.sub('[^A-Za-z0-9]+', '', t)
print(t1)
# if len(t1.strip()) > 0:
#     ntxt.append(t1)


# In[ ]:




